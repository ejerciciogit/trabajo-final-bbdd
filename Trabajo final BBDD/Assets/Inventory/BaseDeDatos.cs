﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.SqliteClient;
using System;

public class BaseDeDatos : MonoBehaviour
{
    public SqliteConnection OpenConexion()
    {
        string path = Application.dataPath;
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Inventory/Plugins/Inventario.db");
        conexion.Open();
        return conexion;
    }
    public void CrearTabla()
    {
        string creacion = "CREATE TABLE IF NOT EXISTS Inventario (ItemType string(15), amount numeric(3))";
        SqliteCommand cmd = new SqliteCommand(creacion, OpenConexion());
        cmd.ExecuteNonQuery();

        OpenConexion().Close();
    }
    public void CrearVista()
    {
        string vista = "CREATE VIEW IF NOT EXISTS ObjetosEnInventario AS SELECT COUNT(DISTINCT ItemType) FROM Inventario; ";
        SqliteCommand cmd2 = new SqliteCommand(vista, OpenConexion());
        cmd2.ExecuteNonQuery();

        OpenConexion().Close();
    }
    public void CrearTrigger()
    {
        string trigger = "CREATE TRIGGER IF NOT EXISTS objetosactualizados BEFORE UPDATE ON Inventario FOR EACH ROW begin INSERT INTO Borrados VALUES(OLD.ItemType, OLD.amount); end; ";
        SqliteCommand cmd3 = new SqliteCommand(trigger, OpenConexion());
        cmd3.ExecuteNonQuery();

        OpenConexion().Close();
    }



    public void añadirItem(Item item)
    {

        string insert = @"INSERT INTO Inventario (ItemType, amount) VALUES (@ItemType, @amount)";
        SqliteCommand cmd = new SqliteCommand(insert, OpenConexion());

        cmd.Parameters.Add(new SqliteParameter("@ItemType", item.itemType));
        cmd.Parameters.Add(new SqliteParameter("@amount", item.amount));
        cmd.ExecuteNonQuery();
        OpenConexion().Close();

    }
    public void EliminarObjeto(Item item)
    {
        string consulta = "DELETE FROM Inventario WHERE ItemType = @ItemType";
        SqliteCommand cmd = new SqliteCommand(consulta, OpenConexion());
        cmd.Parameters.Add(new SqliteParameter("@ItemType", item.itemType));
        cmd.ExecuteNonQuery();
    }    public void ObjetosTotales()
    {
        string vista = "SELECT * FROM ObjetosEnInventario; ";
        SqliteCommand cmd = new SqliteCommand(vista, OpenConexion());
        SqliteDataReader datos = cmd.ExecuteReader();
        while (datos.Read())
        {
            string objetos = Convert.ToString(datos[0]);
            string mensaje = "Numero de Objetos Unicos en Inventario:" + objetos;
            Debug.Log(mensaje);
        }
    }
}